import React, { PureComponent } from "react";
import SingleComment from "./SingleComment.jsx";
import MyComments from "./MyComments.jsx";
import { connect } from "react-redux";

class Comments extends PureComponent {
  state = {
    id: this.props.valueOfId,
    data: <div> Comments Loading</div>,
    comments: "",
    isSignedIn: false
  };

  componentDidMount() {
    this.updateUi(this.state.id);
  }

  updateUi = id => {
    fetch(
      "https://api.github.com/repos/freecodecamp/freecodecamp/issues/" +
        String(this.state.id)
    )
      .then(res => res.json())
      .then(data1 => {
        // console.log(data);
        this.setState({
          title: data1["title"],
          comments: <h4>Loading Comments...</h4>
        });

        fetch(data1["comments_url"])
          .then(res => res.json())
          .then(data2 => {
            if (data2.length !== 0) {
              this.setState({
                comments: data2.map((_, index) => {
                  return (
                    <SingleComment
                      imgUrl={data2[index]["user"]["avatar_url"]}
                      title={data2[index]["title"]}
                      bodyOfIssue={data2[index]["body"]}
                      userName={data2[index]["user"]["login"]}
                      deleteComment={() =>
                        this.deleteCommentFn(
                          data2[index]["user"]["login"],
                          data2[index]["id"]
                        )
                      }
                    />
                  );
                })
              });
            } else {
              this.setState({
                comments: <h4>No Comments</h4>
              });
            }
          });
      });
  };
  postCommentFn = () => {
    console.log(this.state.id);
    console.log(this.props.token);
    let textFromComment = document.getElementById("addComment");
    console.log(textFromComment.value);
    console.log("Bearer " + this.props.token);
    console.log(
      "https://api.github.com/repos/freecodecamp/freecodecamp/issues/" +
        this.state.id +
        "/comments?access_token=" +
        this.props.token
    );
    // https://github.com/madurachanna/testremp/issues/1

    // "http://api.github.com/repos/freecodecamp/freecodecamp/issues/" +
    //     this.state.id +
    //     "/comments"
    fetch(
      "http://api.github.com/repos/freecodecamp/freecodecamp/issues/" +
        this.state.id +
        "/comments?access_token=" +
        this.props.token,
      {
        method: "post",
        headers: new Headers({
          Authorization: "Bearer ce661e5074ae8e83b191d41f5322195a12639c66"
        }),
        body: JSON.stringify({
          body: textFromComment.value
        })
      }
    )
      .then(res => res.json())
      .then(data => {
        console.log("post id " + data["id"]);
        this.updateUi(this.state.id);
      });
    textFromComment.value = "";
  };

  // "Bearer ce661e5074ae8e83b191d41f5322195a12639c66"
  deleteCommentFn = (val1, val2) => {
    console.log(val1);
    console.log(typeof val2);
    console.log(val2);

    if (this.props.isSignedIn === false) {
      alert("You need to sign in!!");
    } else if (val1 === "madurachanna") {
      console.log("my comment");
      console.log(this.props.userToken);
      // DELETE /repos/:owner/:repo/issues/comments/:comment_id
      // https://api.github.com/repos/freecodecamp/freecodecamp/issues/comments/483962729
      fetch(
        "https://api.github.com/repos/freecodecamp/freecodecamp/issues/comments/" +
          val2,
        {
          method: "delete",
          headers: new Headers({
            Authorization: "Bearer ce661e5074ae8e83b191d41f5322195a12639c66"
          })
        }
      ).then(() => {
        this.updateUi(this.state.id);
      });
    } else {
      console.log("not my comment");
      alert("Not your comment, Cannot Delete");
    }
  };

  render() {
    // console.log("comment render");
    // console.log(this.props.token);

    return (
      <div className="commentBody">
        {this.props.isSignedIn ? (
          <div>
            <div id="specificTitle">
              <h2>{this.state.title}</h2>
            </div>
            {this.state.comments}
            <MyComments
              isueId={this.state.id}
              dis={false}
              postComment={this.postCommentFn}
              pH="Leave Your Comment!!!"
            />
          </div>
        ) : (
          <div>
            <div id="specificTitle">
              <h2>{this.state.title}</h2>
            </div>
            {this.state.comments}
            <MyComments
              isueId={this.state.id}
              dis={true}
              postComment={this.postCommentFn}
              pH="Login to Comment!!!"
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.isSignedIn,
    token: state.token
  };
};

export default connect(mapStateToProps)(Comments);
