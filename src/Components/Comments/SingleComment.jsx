import React, { PureComponent } from "react";
import "./Comments.css";

class SingleComment extends PureComponent {
  state = {};
  render() {
    return (
      <div className="infoOuter">
        <div className="userImage">
          <img src={this.props.imgUrl} alt="" />
        </div>
        <div className="informationBox">
          <div className="userDetails">
            <span>{this.props.userName}</span>
            <button onClick={this.props.deleteComment}>X</button>
          </div>

          <div className="bodyOfIssue">{this.props.bodyOfIssue}</div>
        </div>
      </div>
    );
  }
}

export default SingleComment;
