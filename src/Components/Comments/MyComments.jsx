import React, { PureComponent } from "react";
import "./Comments.css";

class MyComments extends PureComponent {
  state = {
    id: this.props.isueId,
    commentId: 0
  };

  render() {
    return (
      <div className="infoOuter">
        <div className="userImage userImageV2" />
        <div className="informationBox">
          <div className="userDetails">
            <span>Add Comments</span>
          </div>

          <div className="bodyOfIssue bodyOfIssueV2">
            <textarea
              id="addComment"
              value={this.props.val}
              disabled={this.props.dis}
              placeholder = {this.props.pH}
            />
            <button disabled={this.props.dis} onClick={this.props.postComment}>
              Send
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default MyComments;

// https://api.github.com/repos/freecodecamp/freecodecamp/issues/35842/comments
