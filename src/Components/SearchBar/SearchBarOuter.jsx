import React, { PureComponent } from "react";
import "./SearchBar.css";
import SearchBox from "./SearchBox.jsx";

class SearchBarOuter extends PureComponent {
  state = {};
  render() {
    return (
      <div className="searchBarOuter">
        <SearchBox searchBody={this.props.search2} />
      </div>
    );
  }
}

export default SearchBarOuter;
