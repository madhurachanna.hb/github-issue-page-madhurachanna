import React, { PureComponent } from "react";
import "./SearchBar.css";

class SearchBox extends PureComponent {
  state = {};

  render() {
    return (
      <input type="text" id="searchBox" onKeyDown={this.props.searchBody} />
    );
  }
}

export default SearchBox;
