import React, { PureComponent } from "react";
import "./Body.css";

class BodySingleElement extends PureComponent {
  state = {};

  render() {
    const label = this.props.data.map((val, inx) => {
      let color = "#" + val["color"];
      let style = {
        backgroundColor: color,
        borderRadius: "5px",
        marginLeft: "2px",
        marginRight: "2px",
        padding: "1px"
      };
      return <span style={style}>{val["name"]}</span>;
    });

    return (
      <div className="bodySingleElement">
        <div className="warningImg">
          <img src={require("../Images/warning.png")} alt="" />
        </div>

        <div className="theContent">
          <span>
            <b>{this.props.titleContent}</b>
          </span>
          {label}

          <div className="issueDetails">
            <span>#</span>
            <span>{this.props.number}</span>
            <span> opend by </span>
            <span>{this.props.diffDate}</span>
            <span> {this.props.userName}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default BodySingleElement;
