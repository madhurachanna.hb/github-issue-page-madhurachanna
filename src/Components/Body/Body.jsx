import React, { PureComponent } from "react";
import "./Body.css";
import BodyHeader from "./BodyHeader.jsx";
import Paginate from "../Pagination/Pagination.jsx";
import { connect } from "react-redux";
// import TimesFromFile from "./Timer.jsx";
import SearchBarOuter from "../SearchBar/SearchBarOuter.jsx";
let gitHubData = [];

class Body extends PureComponent {
  componentDidMount() {
    console.log("component did mount");
    console.log(this.props.d);
    this.updateUi(this.props.appJsData);
    console.log(this.props.appJsData);
  }

  updateUi = lmnop => {
    console.log("updateUiFn---------------------------");
    console.log(lmnop);
    fetch(lmnop)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        gitHubData = data;
        this.props.setGitHubData(data);
        this.props.setSingleElement(data);
      });
  };

  // if (gitHubData[i]["labels"][j]["name"] === key)/

  filterFn = event => {
    // console.log(event.target.name);
    let filterdArray = [];
    let key = event.target.value;
    console.log("key = " + key);
    console.log(typeof key);
    if (event.target.name === "labels") {
      console.log("in labels");
      for (let i = 0; i < gitHubData.length; i++) {
        if (gitHubData[i]["labels"].length !== 0) {
          for (let j = 0; j < gitHubData[i]["labels"].length; j++) {
            if (gitHubData[i]["labels"][j]["name"] === key) {
              console.log(gitHubData[i]["labels"][j]["name"]);
              let obj = {
                title: gitHubData[i]["title"],
                id: i,
                number: gitHubData[i]["number"],
                user: gitHubData[i]["user"]["login"],
                // diffDate: TimesFromFile[i],
                labels: gitHubData[i]["labels"],
                updated_at: gitHubData[i]["updated_at"]
              };
              filterdArray.push(obj);
            }
          }
        }
      }
    }
    if (event.target.name === "userName") {
      console.log("in userName");
      if (key !== "Authors") {
        for (let i = 0; i < gitHubData.length; i++) {
          if (gitHubData[i]["user"]["login"] === key) {
            let obj = {
              title: gitHubData[i]["title"],
              id: i,
              number: gitHubData[i]["number"],
              user: gitHubData[i]["user"]["login"],
              // diffDate: TimesFromFile[i],
              labels: gitHubData[i]["labels"],
              updated_at: gitHubData[i]["updated_at"]
            };
            filterdArray.push(obj);
          }
        }
      } else {
        filterdArray = gitHubData;
      }
    }

    this.props.setSingleElement(filterdArray);
  };

  sortFn = event => {
    let key = event.target.value;
    let sortedArray = [];

    console.log(key);
    for (let i = 0; i < gitHubData.length; i++) {
      let obj = {
        title: gitHubData[i]["title"],
        id: i,
        number: gitHubData[i]["number"],
        // diffDate: TimesFromFile[i],
        rawDate: new Date(gitHubData[i]["updated_at"]).getTime() / 1000,
        labels: gitHubData[i]["labels"],
        updated_at: gitHubData[i]["updated_at"],
        user: {
          login: gitHubData[i]["user"]["login"]
        }
      };
      console.log(obj["diffDate"]);
      sortedArray.push(obj);
    }

    if (key === "title") {
      sortedArray.sort((a, b) =>
        a.title.toLowerCase() > b.title.toLowerCase()
          ? 1
          : b.title.toLowerCase() > a.title.toLowerCase()
          ? -1
          : 0
      );
    }

    if (key === "user") {
      sortedArray.sort((a, b) =>
        a.user.toLowerCase() > b.user.toLowerCase()
          ? 1
          : b.user.toLowerCase() > a.user.toLowerCase()
          ? -1
          : 0
      );
    }

    if (key === "Latest") {
      sortedArray = sortedArray.sort((a, b) => b["rawDate"] - a["rawDate"]);
    }

    if (key === "Oldest") {
      sortedArray = sortedArray.sort((a, b) => a["rawDate"] - b["rawDate"]);
    }
    this.props.setSingleElement(sortedArray);
  };

  searchFn = event => {
    if (event.which === 13) {
      let searchedArray = [];
      console.log(event.target.value);
      for (let i = 0; i < gitHubData.length; i++) {
        if (gitHubData[i]["title"].indexOf(event.target.value) >= 0) {
          let obj = {
            title: gitHubData[i]["title"],
            id: i,
            number: gitHubData[i]["number"],
            user: gitHubData[i]["user"]["login"],
            // diffDate: TimesFromFile[i],
            labels: gitHubData[i]["labels"],
            updated_at: gitHubData[i]["updated_at"]
          };
          searchedArray.push(obj);
        }
      }
      this.props.setSingleElement(searchedArray);
    }
  };

  handlePageClickFn = data => {
    let selected = data.selected + 1;
    console.log(selected + "--------------------------------------");
    console.log(this.props.d);

    this.props.d.history.push("/page/" + selected);
    let url =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=" +
      String(selected);
    this.updateUi(url);
  };

  render() {
    console.log("rendering");
    return (
      <div style={{ minHeight: "600px" }}>
        <SearchBarOuter search2={event => this.searchFn(event)} />
        <BodyHeader sort={this.sortFn} filter={event => this.filterFn(event)} />
        {this.props.SingleElement}
        <Paginate handlePageClick={this.handlePageClickFn} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    SingleElement: state.SingleElement,
    appJsData: state.appJsData,
    multiTags: state.multiTags
  };
};

const mapDispachToProps = dispatch => {
  return {
    setSingleElement: data =>
      dispatch({ type: "SINGLE_ELEMENT", value: 1, gitHubData: data }),
    onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 }),
    setGitHubData: data => dispatch({ type: "TIME_DATA_UPDATE", value: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Body);
