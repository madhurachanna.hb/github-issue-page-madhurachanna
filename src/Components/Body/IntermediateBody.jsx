import React, { PureComponent } from "react";
import "./Body.css";
import BodyHeader from "./BodyHeader.jsx";
import BodySingleElement from "./BodySingleElement.jsx";
import TimesFromFile from "./Timer.jsx";
import { BrowserRouter, Route, Link } from "react-router-dom";
import SearchBarOuter from "../SearchBar/SearchBarOuter.jsx";
import Body from "./Body.jsx";
let gitHubData = [
  {
    url: "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/35765",
    repository_url: "https://api.github.com/repos/freeCodeCamp/freeCodeCamp",
    labels_url:
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/35765/labels{/name}",
    comments_url:
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/35765/comments",
    events_url:
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/35765/events",
    html_url: "https://github.com/freeCodeCamp/freeCodeCamp/issues/35765",
    id: 429387673,
    node_id: "MDU6SXNzdWU0MjkzODc2NzM=",
    number: 35765,
    title:
      "Problem with Guide solution and challenge solution for Reuse Patterns Using Capture Groups challenge",
    user: {
      login: "RandellDawson",
      id: 5313213,
      node_id: "MDQ6VXNlcjUzMTMyMTM=",
      avatar_url: "https://avatars2.githubusercontent.com/u/5313213?v=4",
      gravatar_id: "",
      url: "https://api.github.com/users/RandellDawson",
      html_url: "https://github.com/RandellDawson",
      followers_url: "https://api.github.com/users/RandellDawson/followers",
      following_url:
        "https://api.github.com/users/RandellDawson/following{/other_user}",
      gists_url: "https://api.github.com/users/RandellDawson/gists{/gist_id}",
      starred_url:
        "https://api.github.com/users/RandellDawson/starred{/owner}{/repo}",
      subscriptions_url:
        "https://api.github.com/users/RandellDawson/subscriptions",
      organizations_url: "https://api.github.com/users/RandellDawson/orgs",
      repos_url: "https://api.github.com/users/RandellDawson/repos",
      events_url: "https://api.github.com/users/RandellDawson/events{/privacy}",
      received_events_url:
        "https://api.github.com/users/RandellDawson/received_events",
      type: "User",
      site_admin: false
    },
    labels: [
      {
        id: 1130338882,
        node_id: "MDU6TGFiZWwxMTMwMzM4ODgy",
        url:
          "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/labels/language:%20English",
        name: "language: English",
        color: "8ed9e2",
        default: false
      },
      {
        id: 252661769,
        node_id: "MDU6TGFiZWwyNTI2NjE3Njk=",
        url:
          "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/labels/scope:%20curriculum",
        name: "scope: curriculum",
        color: "9631e2",
        default: false
      }
    ],
    state: "open",
    locked: false,
    assignee: null,
    assignees: [],
    milestone: null,
    comments: 1,
    created_at: "2019-04-04T17:03:13Z",
    updated_at: "2019-04-04T22:28:19Z",
    closed_at: null,
    author_association: "MEMBER",
    body:
      'Below are the current instructions for the [Reuse Patterns Using Capture Groups](https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/regular-expressions/reuse-patterns-using-capture-groups) challenge.\r\n\r\n> Use capture groups in reRegex to match numbers that are repeated only three times in a string, each separated by a space.\r\n\r\nReading through common questions on the forum related to this challenge, there probably should be some modifications to these instructions.  \r\n\r\n1. It should made clearer that the numbers should appear consecutively separated by a space vs just repeated three times.  Why?  Because in the following string, the same number is repeated 3 times and the numbers are separated by a space, but they are not consecutive.\r\n```\r\n"42 32 42 42"\r\n```\r\n2. Even if we fix the wording as described above, there is still the issue that the following strings  should match, because all 3 of the "42"s are consecutive and separated by a space.\r\n```\r\n"100 42 42 42"\r\n"42 42 42 100"\r\n"100 42 42 42 100"\r\n"42 42 42 100 100 100"\r\n```\r\nAll the above would fail the currently "accepted" regex solutions (both seen below)\r\n```\r\n/^(\\d+)\\s\\1\\s\\1$/\r\n\r\nor\r\n\r\n/^(\\d+) \\1 \\1$/\r\n```'
  }
];

let x = gitHubData.map((_, index) => {
  return gitHubData[index]["number"];
});
let Labels = gitHubData.map((_, index) => {
  return gitHubData[index]["labels"];
});

// let abc;

class IntermediateBody extends PureComponent {
  constructor(props) {
    super(props);
    console.log("constructor");
  }
  state = {
    SingleElement: "dfdf",

    appJs: this.props.dataFetchedFrom
  };

  componentDidMount() {
    return <Body dataFetchedFromApi={this.state.appJs} />;
  }

  filterFn = event => {};

  sortFn = event => {};

  searchFn = event => {};

  render() {
    console.log("rendering");
    return <div />;
  }
}

export default IntermediateBody;
