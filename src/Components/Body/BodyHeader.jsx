import React, { PureComponent } from "react";
import "./Body.css";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import Chip from "@material-ui/core/Chip";
import { connect } from "react-redux";
import { relative } from "path";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    maxWidth: 300
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: theme.spacing.unit / 4
  },
  noLabel: {
    marginTop: theme.spacing.unit * 3
  }
});

const names = [
  "language: English",
  "scope: curriculum",
  "status: blocked",
  "status: merge conflict",
  "status: work in progress",
  "status: need update",
  "language: Chinese"
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

let key = [];

class BodyHeader extends PureComponent {
  state = {
    name: []
  };

  handleChange = event => {
    let obj = { name: event.target.value };
    console.log(obj);
    this.setState({ name: event.target.value });
    console.log("state===============================");
    console.log(this.state.name);
    // console.log(event.target.value);
    this.props.setTags(event.target.value);
    //=============================================================

    let filterdArray = [];

    // let set = new Set(obj["name"][0]);

    key.push(obj["name"][0]);
    console.log("key===========================");
    console.log(key);
    let set = new Set(key);

    key = Array.from(set);

    // let key =  event.target.value;
    // let key = obj["name"][0];
    // if (this.props.gitHubData[i]["labels"][j]["name"] === key)
    console.log("key===========================");
    console.log(key);
    console.log("set=====================");
    console.log(set);
    key = obj["name"];
    if (event.target.name === "labels") {
      if (key.length !== 0) {
        console.log("in labels");
        for (let i = 0; i < this.props.gitHubData.length; i++) {
          if (this.props.gitHubData[i]["labels"].length !== 0) {
            for (
              let j = 0;
              j < this.props.gitHubData[i]["labels"].length;
              j++
            ) {
              if (key.includes(this.props.gitHubData[i]["labels"][j]["name"])) {
                console.log(this.props.gitHubData[i]["labels"][j]["name"]);
                let obj = {
                  title: this.props.gitHubData[i]["title"],
                  id: i,
                  number: this.props.gitHubData[i]["number"],
                  user: this.props.gitHubData[i]["user"]["login"],
                  // diffDate: TimesFromFile[i],
                  labels: this.props.gitHubData[i]["labels"],
                  updated_at: this.props.gitHubData[i]["updated_at"]
                };
                filterdArray.push(obj);
              }
            }
          }
        }
      } else {
        filterdArray = this.props.gitHubData;
      }
    }
    this.props.setSingleElement(filterdArray);

    //================================================================
  };

  fruits = ["Apples", "Oranges", "Pears"];

  onSelectionsChange = selectedFruits => {
    // selectedFruits is array of { label, value }
    this.setState({ selectedFruits });
  };

  render() {
    return (
      <div className="bodyHeader">
        <select onChange={this.props.sort} className="dropDowns">
          <option value="Sort">Sort</option>
          <option value="Latest">Latest</option>
          <option value="title">Title</option>
          <option value="Oldest">Oldest</option>
        </select>
        <select
          onChange={this.props.filter}
          name="userName"
          className="dropDowns"
        >
          <option value="Authors">Authors</option>
          <option value="StanimalTheMan ">StanimalTheMan </option>
          <option value="candacej97">candacej97 </option>
          <option value="mhkafadar">mhkafadar </option>
          <option value="alypilkons">alypilkons </option>
          <option value="sanghyundev">sanghyundev </option>
          <option value="sayansur25">sayansur25 </option>
          <option value="Wocanilo">Wocanilo </option>
          <option value="sil3nthill">sil3nthill </option>
          <option value="DanieSegarra36">DanieSegarra36 </option>
        </select>

        <FormControl style={{ position: "relative", height: "50px" }}>
          {/* <InputLabel htmlFor="select-multiple-checkbox">Tag</InputLabel> */}
          <span style={{ position: "absolute", top: "15px" }}>Labels</span>
          <Select
            style={{ height: "50px", marginLeft: "50px" }}
            multiple
            value={this.state.name}
            onChange={this.handleChange}
            // onChange={this.props.filter}
            input={<Input id="select-multiple-checkbox" />}
            renderValue={selected => selected.join(", ")}
            MenuProps={MenuProps}
            name="labels"
          >
            {names.map(name => (
              <MenuItem key={name} value={name}>
                <Checkbox checked={this.state.name.indexOf(name) > -1} />
                <ListItemText primary={name} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {/* <button id="clearButton">Clear</button> */}
      </div>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     setTags: () => dispatch({ type: "SET_TAGS" }),
//     onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 })
//   };
// };

const mapStateToProps = state => {
  return {
    SingleElement: state.SingleElement,
    appJsData: state.appJsData,
    gitHubData: state.GitHubData,
    multiTags: state.multiTags
  };
};
const mapDispachToProps = dispatch => {
  return {
    setSingleElement: data =>
      dispatch({ type: "SINGLE_ELEMENT", value: 1, gitHubData: data }),
    setTags: data => dispatch({ type: "SET_TAGS", value: data })
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(BodyHeader);
