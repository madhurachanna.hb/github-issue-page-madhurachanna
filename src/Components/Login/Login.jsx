import React, { Component } from "react";
import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import "./Login.css";
import { connect } from "react-redux";

firebase.initializeApp({
  apiKey: "AIzaSyBsKsfSiPA0c3EJm8R96Er6QYDwDH7XIDc",
  authDomain: "github-issue.firebaseapp.com"
});

class Login extends Component {
  state = { isSignedIn: false };

  uiConfig = {
    signInFlow: "popup",
    signInOptions: [firebase.auth.GithubAuthProvider.PROVIDER_ID],
    callbacks: {
      signInSuccessWithAuthResult: authRes => {
        let token = authRes["credential"]["accessToken"];
        sessionStorage.setItem("token", token);
        console.log(token);
        console.log(authRes);
        console.log(authRes["additionalUserInfo"]["username"]);
        let val = true;
        this.props.setUserName(authRes["additionalUserInfo"]["username"]);
        this.props.onSignIn(val);
        sessionStorage.setItem(
          "loginName",
          authRes["additionalUserInfo"]["username"]
        );
        this.props.setToken(token);
      }
    }
  };

  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
      this.props.onSignIn(!!user);
      this.props.setUserName(sessionStorage.getItem("loginName"));
      this.setState({ isSignedIn: !!user });
    });
    // this.setState({ isSignedIn: !!user }));
    console.log(this.state.isSignedIn);
  }

  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  onLogOut = () => {
    firebase.auth().signOut();
    let val = false;
    sessionStorage.removeItem("token");
    this.props.onSigOut(val);
  };

  render() {
    return (
      <div>
        {this.props.isSignedIn ? (
          <div>
            <span id="userName"> Hi, {this.props.loginName}</span>

            <span>
              <button className="signOutButton" onClick={() => this.onLogOut()}>
                Sign out!
              </button>
            </span>
          </div>
        ) : (
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.isSignedIn,
    loginName: state.loginName
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSignIn: val => dispatch({ type: "LOG_IN", value: val }),
    onSigOut: val => dispatch({ type: "LOG_OUT", value: val }),
    setToken: token => dispatch({ type: "TOKEN", value: token }),
    setUserName: val => dispatch({ type: "USER_NAME", value: val })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
