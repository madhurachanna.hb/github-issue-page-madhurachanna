import React, { PureComponent } from "react";
import "./Header.css";

class HeaderLogo extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerLogo">
        <img src={require("../Images/logo.png")} alt="logo" />
      </div>
    );
  }
}

export default HeaderLogo;
