import React, { PureComponent } from "react";
import "./Header.css";
import HeaderLogo from "./HeaderLogo.jsx";

class HeaderInner extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerInner">
        <HeaderLogo />
      </div>
    );
  }
}

export default HeaderInner;
