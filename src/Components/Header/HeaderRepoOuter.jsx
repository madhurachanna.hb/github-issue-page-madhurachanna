import React, { PureComponent } from "react";
import "./Header.css";
import HeaderRepoInner from "./HeaderRepoInner.jsx";

class HeaderRepoOuter extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerRepoOuter ">
        <HeaderRepoInner />
      </div>
    );
  }
}

export default HeaderRepoOuter;
