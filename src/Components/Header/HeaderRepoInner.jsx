import React, { PureComponent } from "react";
import "./Header.css";
import HeaderRepoTitle from "./HeaderRepoTitle.jsx";
import HeaderIssueBar from "./HeaderIssueBar.jsx";

class HeaderRepoInner extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerRepoInner">
        <HeaderRepoTitle />
        <HeaderIssueBar />
      </div>
    );
  }
}

export default HeaderRepoInner;
