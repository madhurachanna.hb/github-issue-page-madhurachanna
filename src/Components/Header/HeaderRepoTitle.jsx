import React, { PureComponent } from "react";
import "./Header.css";

class HeaderRepoTitle extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerRepoTitle">
        <span id="fccText1">freeCodeCamp / </span>
        <span id="fccText2">freeCodeCamp</span>
      </div>
    );
  }
}

export default HeaderRepoTitle;
