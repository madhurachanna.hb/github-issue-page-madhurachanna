import React, { PureComponent } from "react";
import "./Header.css";
import HeaderInner from "./HeaderInner.jsx";
import HeaderRepoOuter from "./HeaderRepoOuter.jsx";

class HeaderOuter extends PureComponent {
  state = {};
  render() {
    console.log("in header");
    return (
      <div className="headerOuter">
        <HeaderInner />
        <HeaderRepoOuter />
      </div>
    );
  }
}

export default HeaderOuter;
