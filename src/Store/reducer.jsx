import React from "react";
import BodySingleElement from "../Components/Body/BodySingleElement.jsx";
import { Link } from "react-router-dom";
import moment from "moment";

const initialState = {
  appJsData: "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues",
  isSignedIn: false,
  GitHubData: 0,
  token: "",
  loginName: "",
  multiTags: [],
  SingleElement: (
    <div className="loadingBox">
      <h1>Loading...</h1>
    </div>
  )
};

let currentTimeFormated = () => {
  var date = new Date();
  let currentDate = date.getDate(); // Get current date
  let month = date.getMonth() + 1; // current month
  let year = date.getFullYear();
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  if (month < 10) {
    month = "0" + month;
  }
  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  return (
    "" +
    year +
    "-" +
    month +
    "-" +
    currentDate +
    "T" +
    hours +
    ":" +
    minutes +
    ":" +
    seconds +
    "Z"
  );
};

let timeFormats = GitHubData => {
  console.log(typeof GitHubData);

  let now = currentTimeFormated();
  let datesArray = [];

  for (let i = 0; i < GitHubData.length; i++) {
    let then = GitHubData[i]["updated_at"];

    let ms = moment(now, "YYYY-MM-DDTHH:mm:ss").diff(
      moment(then, "YYYY-MM-DDTHH:mm:ss")
    );
    let d = moment.duration(ms);
    let s = Math.floor(d.asDays()) + moment.utc(ms).format(":hh:mm");
    datesArray.push(s);
  }
  return datesArray;
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };

  switch (action.type) {
    case "LOG_IN":
      newState.isSignedIn = action.value;
      break;

    case "USER_NAME":
      newState.loginName = action.value;
      break;

    case "TIME_DATA_UPDATE":
      console.log("Time = ");
      console.log(action.value);
      newState.GitHubData = action.value;
      break;

    case "TOKEN":
      console.log(action.value);
      newState.token = action.value;
      break;

    case "LOG_OUT":
      newState.isSignedIn = action.value;
      break;

    case "SET_TAGS":
      // newState.multiTags = action.value;
      newState.multiTags = newState.multiTags.concat(action.value);
      console.log(newState.multiTags);
      break;

    case "SINGLE_ELEMENT":
      let data = action.gitHubData;
      let Times = timeFormats(data);
      console.log("this  is single element from reducer");
      newState.SingleElement = data.map((_, index) => {
        console.log(data[index]["user"]["login"]);

        return (
          <Link
            to={`/issues/${data[index]["number"]}`}
            style={{ textDecoration: "none", color: "black" }}
          >
            <BodySingleElement
              key={index}
              id={index}
              disp={() => this.dispDetailsFn(index)}
              titleContent={data[index]["title"]}
              number={data[index]["number"]}
              userName={data[index]["user"]["login"]}
              data={data[index]["labels"]}
              diffDate={Times[index]}
            />
          </Link>
        );
      });
  }
  return newState;
};

export default reducer;
