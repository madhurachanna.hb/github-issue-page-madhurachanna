import React, { Component } from "react";
import "./App.css";
import HeaderOuter from "./Components/Header/HeaderOuter.jsx";
import Body from "./Components/Body/Body.jsx";
import { BrowserRouter, Route } from "react-router-dom";
import Comments from "./Components/Comments/Comments.jsx";
import Login from "./Components/Login/Login.jsx";
import { connect } from "react-redux";

class App extends Component {
  state = {};
  dispFn = dat => {
    console.log(dat);
    return (
      <Body
        urlOfApi="https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues"
        d={dat}
      />
    );
  };

  commentsFn = ({ match }) => {
    console.log("inside");
    // console.log(dat);
    return <Comments valueOfId={match.params.num} />;
  };

  testDispFn = dat => {
    console.log("page chaged---------------------------");
    console.log("button is " + dat.match.params.pageNum);
    let url =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=" +
      dat.match.params.pageNum;
    console.log("url sent = " + url);
    return <Body d={dat} />;
  };

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <div>
            <Login />
            <HeaderOuter />
          </div>
          <Route path="/" exact render={this.dispFn} />
          <Route path="/issues/:num" exact render={this.commentsFn} />
          <Route path="/page/:pageNum" exact render={this.testDispFn} />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    SingleElement: state.SingleElement
  };
};

const mapDispachToProps = dispatch => {
  return {
    setSingleElement: () => dispatch({ type: "AGE_UP", value: 1 })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);
